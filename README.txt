Allow users to create context reactions that hide the toolbar.

To use this module create a new context, assign any relevant conditions and then
add "Hide Toolbar" as a reaction, being careful to check the "Hide toolbar"
checkbox in the reaction settings form.
